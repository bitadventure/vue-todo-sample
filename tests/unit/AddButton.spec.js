import { mount } from '@vue/test-utils';
import AddButton from '@/components/AddButton';

describe('AddButton.vue', () => {
    it('add button exist on the page', () => {

        const wrapper = mount(AddButton);

        const count = wrapper.findAll('button').length;

        expect(count).toBe(1)
    })
});
