import { mount } from '@vue/test-utils';
import TodoItem from '@/components/TodoItem';

describe('TodoItem.vue', () => {
    it('todo item correct props', () => {
        const todo = {
            name: "2",
            isReady: false
        };
        const index = 99;

        const wrapper = mount(TodoItem, {
            propsData: {
                value: todo,
                index
            }
        });

        expect(wrapper.props().value).toBe(todo);
        expect(wrapper.props().index).toBe(99);
    });

    it('todo item text', () => {
        const todo = {
            name: "Hello world",
            isReady: false
        };
        const index = 99;

        const wrapper = mount(TodoItem, {
            propsData: {
                value: todo,
                index
            }
        });

        expect(wrapper.text()).toBe("Hello world");
    });
});
