import { mount } from '@vue/test-utils';
import List from '@/components/List';

describe('List.vue', () => {
    it('renders li for each item in todos', () => {
        const todos = [
            {
                name: "1",
                isReady: false
            },
            {
                name: "2",
                isReady: false
            }
        ];

        const wrapper = mount(List, {
            propsData: { todos }
        });

        expect(wrapper.findAll('li')).toHaveLength(todos.length)
    })
});
