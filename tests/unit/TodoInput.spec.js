import { mount } from '@vue/test-utils';
import TodoInput from '@/components/TodoInput';

describe('TodoInput.vue', () => {
    it('input element exist on the page', () => {
       const wrapper = mount(TodoInput,  {
           propsData: { val: "test" }
       });
       const count = wrapper.findAll('input[type="text"]').length;

       expect(count).toBe(1)
    });

    it('correct todo input', () => {
        const wrapper = mount(TodoInput, {
            propsData: { val: "todo todo" }
        });

        const value = wrapper.find('input').element.value;

        expect(value).toBe("todo todo")
    })
});
