const { Router } = require("express");
const functionHelpers = require("firebase-functions-helper");

const collectionName = "todos";

module.exports = db => {
  const router = Router();

  router.get("/", (req, resp) => {
    const todos = [];

    functionHelpers.firestore
      .backup(db, collectionName)
      .then(result => {
        const docs = result[collectionName];
        for (const key in docs) {
          if (docs.hasOwnProperty(key)) {
            const obj = {
              name: docs[key].name,
              isReady: docs[key].isReady,
              key
            };

            todos.push(obj);
          }
        }
        resp.status(200).send(todos);
      })
      .catch(err => resp.status(500).send(err));
  });

  router.post("/", (req, resp) => {
    const { body } = req;

    functionHelpers.firestore.createNewDocument(db, collectionName, body);

    resp.send({ success: true });
  });

  router.put("/:id", (req, resp) => {
    const { body, params } = req;

    functionHelpers.firestore.updateDocument(
      db,
      collectionName,
      params.id,
      body
    );

    resp.send({ success: true });
  });

  return router;
};
